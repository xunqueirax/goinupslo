#!/usr/bin/python3

import json
from reportlab.pdfgen import canvas
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm

def printSong(x, y, song):
  c.drawString (x, y, song["title"])
  x += 350
  c.drawCentredString (x, y, song["tone"])
  x += 80
  c.drawRightString (x, y, song["tempo"])
  y -= 7
  c.line(30, y, 580, y)


with open('setlist.json') as f:
  data = json.load(f)

title = data["title"]
actuals = data["actuals"]
tentatives = data["tentatives"]
width, height = A4

pdfmetrics.registerFont(TTFont('LiberationSerif-Regular', '/usr/share/fonts/liberation/LiberationSerif-Regular.ttf'))
#for font in c.getAvailableFonts(): print(font)

c = canvas.Canvas("goinupslo_all.pdf", pagesize=A4)

c.setFont('LiberationSerif-Regular', 30)
c.setTitle(title)
c.drawString(200, 800, title)
c.setLineWidth(2)
c.line(30, 790, 580, 790)

c.setFont('LiberationSerif-Regular', 22)
xSong=100
yLine=750

c.setLineWidth(0.1)
for song in actuals:
  printSong(xSong, yLine, song)
  yLine -= 28

yLine-=20
c.line(30, yLine, 580, yLine)
yLine-=20
for song in tentatives:
  printSong(xSong, yLine, song)
  yLine -= 28

c.save()
